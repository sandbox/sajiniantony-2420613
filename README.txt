SUMMARY
========

The kaltura tag module can be used to get the kaltura items filtered by
a particular tag.
If a tag is not specified,all the items will be loaded.
With this module,the kaltura partner information can be reset.

REQUIREMENTS
============

Kaltura module , Views module , Chaos tools module , Kaltura Media Views module

INSTALLATION
============

Install as usual,
see https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
=============

Configure the module in
Administration -> Configuration -> Media->Kaltura module settings->Kaltura Tag  


Authors/maintainers
===================

Original Author:

Rony George
https://drupal.org/user/337817

Co-Maintainers:

Sajini Antony
https://drupal.org/user/2603884/

Arun M V
https://drupal.org/user/2727679
