<?php
/**
 * @file
 * This file contains the functions for pagination .
 */

/**
 * The class for pagination.
 */
class KalturaTagPagination {
  private $page = 1;
  private $perPage = 10;
  private $showFirstAndLast = TRUE;
  private $maximumLinkstoShow = 5;
  /**
   * The function for pager.
   */
  public function generate($array, $per_page = 10) {
    if (!empty($per_page)) {
      $this->perPage = $per_page;
    }
    if (!empty($_GET['page'])) {
      $this->page = $_GET['page'];
    }
    else {
      $this->page = 1;
    }
    $this->length = count($array);
    $this->pages = ceil($this->length / $this->perPage);
    $this->start  = ceil(($this->page - 1) * $this->perPage);
    return array_slice($array, $this->start, $this->perPage);
  }
  /**
   * The function for links in the pager items.
   */
  public function links() {
    $plinks = array();
    $links = array();
    $slinks = array();
    if (count($_GET)) {
      $query_url = '';
      foreach ($_GET as $key => $value) {
        if ($key != 'page') {
          $query_url .= '&' . $key . '=' . $value;
        }
      }
    }
    if (($this->pages) > 1) {
      if ($this->page != 1) {
        if ($this->showFirstAndLast) {
          $plinks[] = ' <a href="?page=1' . $query_url . '">&laquo;&laquo; First </a> ';
        }
        $plinks[] = ' <a href="?page=' . ($this->page - 1) . $query_url . '">&laquo; Prev</a> ';
      }
      $links = $this->numlinks($this->page, $this->pages, $this->maximumLinkstoShow);
      if ($this->page < $this->pages) {
        $slinks[] = ' <a href="?page=' . ($this->page + 1) . $query_url . '"> Next &raquo; </a> ';
        if ($this->showFirstAndLast) {
          $slinks[] = ' <a href="?page=' . ($this->pages) . $query_url . '"> Last &raquo;&raquo; </a> ';
        }
      }
      return implode(" ", $plinks) . implode(' ', $links) . implode(" ", $slinks);
    }
  }
  /**
   * The function for links in the pager items.
   */
  public function numlinks($pagenum, $maxpage, $pages_visible, $scriptname = "", $get = "") {
    $links = array();
    $i = 1;
    while ($i <= $pages_visible) {
      if ($pagenum - ceil($pages_visible / 2) < 0) {
        if ($i == $pagenum) {
          $links[] = '<b>' . ($pagenum) . '</b>';
        }
        else {
          $links[] = '<a href="' . $this->pageName($i, $scriptname, $get) . '">' . ($i) . '</a>';
        }
      }
      elseif ($pagenum + floor($pages_visible / 2) > $maxpage) {
        if ($maxpage > $pages_visible) {
          $j = $maxpage - $pages_visible + $i;
        }
        else {
          $j = $i;
        }
        if ($j == $pagenum) {
          $links[] = '<b>' . ($pagenum) . '</b>';
        }
        else {
          $links[] = '<a href="' . $this->pageName($j, $scriptname, $get) . '">' . $j . '</a>';
        }
      }
      else {
        if ($i == ceil($pages_visible / 2)) {
          $links[] = '<b>' . ($pagenum) . '</b>';
        }
        else {
          $j = $pagenum - ceil($pages_visible / 2) + $i;
          $links[] = '<a href="' . $this->pageName($j, $scriptname, $get) . '">' . $j . '</a>';
        }
      }
      if ($i == $maxpage) {
        break;
      }
      $i++;
    }
    return $links;
  }
  /**
   * The function for providing page names called in the function numlinks.
   */
  public function pageName($nr, $scriptname, $get = "") {
    $scriptname .= '?page=' . $nr;
    if ($get != '') {
      $scriptname .= '&' . $get;
    }
    return $scriptname;
  }

}
