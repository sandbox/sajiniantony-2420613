<?php
/**
 * @file
 * This file contains the functions listed in Kaltura Tags Modules.
 */

/**
 * Kaltura_tag form to specify the tag.
 */
function kaltura_tag_form($form, &$form_state) {
  $form['kaltura_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Kaltura tag'),
    '#weight' => 0,
    '#default_value' => variable_get('kaltura_tags', ''),
    '#description' => t('Enter the tag to filter kaltura entries for this site.Enter multiple entries separated by commas.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  return $form;
}

/**
 * Callback function for url admin/settings/kaltura/import.
 */
function kaltura_tag_import_entries_page() {
  module_load_include('inc', 'kaltura_tag', 'includes/kaltura_tag_pagination');
  $pagination = new KalturaTagPagination();
  $each_page = 5;
  if (isset($_POST['op'])) {
    if (!empty($_POST['op']) && $_POST['op'] == 'import selected') {
      kaltura_import_entries(filter_xss($_POST['entries_to_import']));
      drupal_goto('admin/config/media/kaltura/import');
    }
  }
  if (variable_get('kaltura_partner_id', '') == '') {
    return t('You have not configured your partner details yet. Import cannot be performed until you setup your partner details');
  }
  list(, $entries_need_import) = kaltura_tag_get_my_entries($each_page);
  $entries_need_import = kaltura_remove_existing_entries($entries_need_import);
  if (count($entries_need_import) == 0) {
    return "No entries need to be imported";
  }
  $entries_need_import_paged = $pagination->generate($entries_need_import, $each_page);
  if (count($entries_need_import_paged) != 0) {
    $pager = '<div class="numbers">' . $pagination->links() . '</div>';
    $ksetings = new KalturaSettings();
    foreach ($entries_need_import_paged as $details) {
      $entries_options[$details['id']] = $details['name'] . ': ' . '(' . $ksetings->media_types_map[$details['type']] . ') - ' . $details['id'];
    }
  }
  $output = drupal_get_form('kaltura_create_entries_form', array($entries_options));
  $output = drupal_render($output);
  return $pager . $output . $pager;
}

/**
 * Helper function to get entries from Kaltura based on tag, if provided.
 */
function kaltura_tag_get_my_entries($page_size) {
  $tags = variable_get('kaltura_tags');
  $is_admin = 1;
  // Should be able to import all entries in the account (admin task).
  $kalturaclient = new KalturaHelpers();
  $kaltura_client = $kalturaclient->getKalturaClient($is_admin);
  $pager_filter = new KalturaFilterPager();
  $kmf = new KalturaBaseEntryFilter();
  $kmf->typeEqual = KalturaEntryType::MEDIA_CLIP;
  $kmf->status = 2;
  $pager_filter->pageIndex = 1;
  $pager_filter->pageSize = $page_size;
  $result = $kaltura_client->baseEntry->listAction($kmf, $pager_filter);
  $pager_filter->pageSize = $result->totalCount;
  $result = $kaltura_client->baseEntry->listAction($kmf, $pager_filter);
  if ($result->totalCount > 0 && !empty($result->objects)) {
    $returned_entries = array();
    if (is_array($result->objects) && count($result->objects)) {
      foreach ($result->objects as $entry) {
        $tags_array = explode(",", $tags);
        $cnt = count($tags_array);
        if ($cnt != 0) {
          for ($i = 0; $i < $cnt; $i++) {
            $tag = trim($tags_array[$i]);
            $entry_tags = explode(",", $entry->tags);
            if (in_array($tag, $entry_tags)) {
              $returned_entries[$entry->id] = array(
                'id' => $entry->id,
                'name' => $entry->name,
                'type' => $entry->mediaType,
                'tags' => $entry->tags,
              );
            }
          }
        }
        else {
          $returned_entries[$entry->id] = array(
            'id' => $entry->id,
            'name' => $entry->name,
            'type' => $entry->mediaType,
            'tags' => $entry->tags,
          );
        }
      }
      $count = count($returned_entries);
      return array($count, $returned_entries);
    }
    else {
      return array(0, array());
    }
  }
}
